# CriOS

## Development info
* OS: Linux
* Bootloader: GRUB (Possibly in the future make a custom bootloader?) (comes with Linux)
* IDE: CLion (can be installed via Ubuntu Software app)
* Emulator: **qemu (important, not Bochs which is used in the littleosbook)** (sudo apt-get install qemu-system-i386)
* Languages: C++/Assembly (sudo apt-get install build-essential nasm)
* Debugger: GDB (should come with Linux)

To run the OS, run the emulate section in the makefile
## Reading material
  * https://littleosbook.github.io/book.pdf
  * https://www.youtube.com/watch?v=FkrpUaGThTQ
  * https://www.youtube.com/watch?v=wz9CZBeXR6U
  * https://www.gnu.org/software/grub/manual/multiboot/multiboot.html#Header-magic-fields
  * https://wiki.osdev.org/Kernel_Debugging#Use_GDB_with_QEMU
  * https://ftp.gnu.org/old-gnu/Manuals/ld-2.9.1/html_chapter/ld_3.html
  * https://makefiletutorial.com/
  * https://drive.google.com/file/d/11xo_6QPcFaXpsWF9ck-0GjoD2ZwXqL3T/view
  * https://wiki.osdev.org/Bare_Bones#Implementing_the_Kernel
  * https://wiki.osdev.org/Serial_Ports
  * https://wiki.osdev.org/C_PlusPlus
  * https://www.win.tue.nl/~aeb/linux/kbd/scancodes-1.html
  * https://wiki.osdev.org/Calling_Global_Constructors
  * https://wiki.osdev.org/Mouse_Input
  * https://wiki.osdev.org/Programmable_Interval_Timer
  * https://www.youtube.com/watch?v=DuqxVmVGQSU

# TODO
* Make Graphics vs Text mode easily switched between, 
  for now Text mode should be used.
* Better C++ Support
  * Add support for destructors https://wiki.osdev.org/C++#Global_objects
  * Add support for exceptions https://wiki.osdev.org/C++#Exceptions
* GCC Cross Compiler
* Heap 
  * Paging 
  * Virtual Memory
* User Mode
  * Process Manager
  * Scheduler
  * Syscalls
* Multithreading
* File System