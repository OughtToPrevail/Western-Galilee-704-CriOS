#include "Shell.h"
#include "System.h"

void Shell::handleKeyPress(const KeyCode& keyCode)
{
	if(keyCode == KeyCode::BACKSPACE)
	{
		this->deleteChar();
	} else if(keyCode.isASCII())
	{
		this->write(keyCode.asASCII());
	}
}

void Shell::write(unsigned char c)
{
	TextCursor& textCursor = System::getSystem().getTextCursor();
	FrameBuffer& buffer = System::getSystem().getFrameBuffer();
	buffer.moveExact(textCursor.getCursorPosition());
	buffer.write(c);
	textCursor.setCursorPosition(buffer.getCurrentPosition());
}

void Shell::deleteChar()
{
	TextCursor& textCursor = System::getSystem().getTextCursor();
	int newPosition = textCursor.getCursorPosition() - 1;
	if(newPosition < 0)
	{
		return;
	}
	textCursor.setCursorPosition(newPosition);
	this->write(' ');
	textCursor.setCursorPosition(newPosition);
}
