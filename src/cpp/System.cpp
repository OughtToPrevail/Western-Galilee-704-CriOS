#include "System.h"
#include "Halt.h"

System::System() : _keyboard(this->_userInputHardware), _mouse(this->_userInputHardware)
{

}

FrameBuffer& System::getFrameBuffer()
{
	return this->_frameBuffer;
}

TextCursor& System::getTextCursor()
{
	return this->_textCursor;
}

UserInputHardware& System::getUserInterruptHardware()
{
	return this->_userInputHardware;
}

Keyboard& System::getKeyboard()
{
	return this->_keyboard;
}

Mouse& System::getMouse()
{
	return this->_mouse;
}

GraphicsManager& System::getGraphicsManager()
{
	return this->_graphicsManager;
}

Shell& System::getShell()
{
	return this->_shell;
}

Logger& System::getLogger()
{
	return this->_logger;
}

ProgrammableIntervalTimer& System::getPIT()
{
	return this->_timer;
}

void System::halt()
{
	Halt_callHalt();
}

