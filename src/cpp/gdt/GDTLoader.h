#ifndef OSTEST_GDTLOADER_H
#define OSTEST_GDTLOADER_H

struct GDT
{
	unsigned short size;
	unsigned int address;
} __attribute__((packed));

struct GDTEntry
{
	unsigned short limit; // 0:15
	unsigned short base;  // 0:15
	unsigned char base1;  // 16:23
	unsigned char accessByte;
	unsigned char limit1:4;  // 16:19
	unsigned char flags:4;
	unsigned char base2; // 24:31
} __attribute__((packed));

extern "C"
{
void GDTLoad_loadGDT(GDT gdt);
};

#define TOTAL_SEGMENTS (3)

class GDTLoader
{
public:
	static unsigned short loadGDT();
private:
	static void loadEmptyGDTEntry(int index, unsigned char accessByte);

	static GDT gdt;
	static GDTEntry gdtTable[TOTAL_SEGMENTS];
};


#endif //OSTEST_GDTLOADER_H
