#include "GDTLoader.h"

// The offset is 16 because it's 8 bytes for each segment and there is a null segment and data segment before
#define CODE_SEGMENT_OFFSET (16)
#define DATA_SEGMENT_ACCESS_BYTE (0b10010010)
#define CODE_SEGMENT_ACCESS_BYTE (0b10011110)

GDT GDTLoader::gdt = GDT();
GDTEntry GDTLoader::gdtTable[TOTAL_SEGMENTS] = {};

unsigned short GDTLoader::loadGDT()
{
	GDTLoader::loadEmptyGDTEntry(1, DATA_SEGMENT_ACCESS_BYTE); // access, data, grows up, writable, kernel level
	GDTLoader::loadEmptyGDTEntry(2, CODE_SEGMENT_ACCESS_BYTE);  // code, kernel, grows up(?) readable

	GDTLoader::gdt.address = (unsigned int) &GDTLoader::gdtTable[0];
	GDTLoader::gdt.size = sizeof(GDTEntry) * TOTAL_SEGMENTS - 1;
	GDTLoad_loadGDT(GDTLoader::gdt);
	return CODE_SEGMENT_OFFSET;
}

void GDTLoader::loadEmptyGDTEntry(int index, unsigned char accessByte)
{
	GDTEntry& entry = GDTLoader::gdtTable[index];
	entry.limit = 0xFFFF;
	entry.base = 0;
	entry.base1 = 0;
	entry.accessByte = accessByte;
	entry.flags = 0b1100; // 4kib blocks  blocks, 32 bit mode
	entry.limit1 = 0xF;
	entry.base2 = 0;
}
