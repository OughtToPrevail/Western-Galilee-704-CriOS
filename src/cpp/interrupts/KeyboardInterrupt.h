#ifndef OSTEST_KEYBOARDINTERRUPT_H
#define OSTEST_KEYBOARDINTERRUPT_H

#include "InterruptHandler.h"

#define KEYBOARD_INTERRUPT_CODE 0x21

INTERRUPT void keyboardInterrupt(struct InterruptFrame* frame);

#endif //OSTEST_KEYBOARDINTERRUPT_H
