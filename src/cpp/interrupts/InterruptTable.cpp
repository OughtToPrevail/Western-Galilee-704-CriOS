#include "InterruptTable.h"
#include "System.h"
#include "PICInterrupts.h"
#include "TimerInterrupt.h"
#include "KeyboardInterrupt.h"
#include "MouseInterrupt.h"

IDTR InterruptTable::idtr = IDTR();
IDTEntry InterruptTable::idt[IDT_SIZE] = {};

INTERRUPT void divisionByZero(InterruptFrame* frame)
{
	(void) frame;
	System::getSystem().getLogger().log(LogType::ERROR, "Division by 0");
	System::halt();
}

void InterruptTable::registerInterrupts(unsigned short cs)
{
	InterruptTable::idtr.base = (unsigned int) &InterruptTable::idt[0];
	InterruptTable::idtr.limit = (unsigned short) sizeof(IDTEntry) * IDT_SIZE - 1;

	loadInterruptEntry(0, &divisionByZero, cs);

	PICInterrupts::getPICInterrupts(); // will initialize the PIC interrupts
	for(int i = PIC1_OFFSET; i < PIC1_OFFSET + PIC_SIZE; i++)
	{
		loadInterruptEntry(i, &pic1Interrupt, cs);
	}
	for(int i = PIC2_OFFSET; i < PIC2_OFFSET + PIC_SIZE; i++)
	{
		loadInterruptEntry(i, &pic2Interrupt, cs);
	}

	loadInterruptEntry(TIMER_INTERRUPT_CODE, &timerInterrupt, cs);
	loadInterruptEntry(KEYBOARD_INTERRUPT_CODE, &keyboardInterrupt, cs);
	loadInterruptEntry(MOUSE_INTERRUPT_CODE, &mouseInterrupt, cs);

	loadInterruptTable(InterruptTable::idtr);

	/*
	// Test for interrupts (division by zero)
	int x = 1;
	int y = 0;
	int z = x / y;
	*/
}

void InterruptTable::loadInterruptEntry(int index, InterruptHandler handler, unsigned short cs)
{
	InterruptTable::idt[index].init(handler, cs);
}
