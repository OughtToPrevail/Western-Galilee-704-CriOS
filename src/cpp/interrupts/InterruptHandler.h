#ifndef OSTEST_INTERRUPTHANDLER_H
#define OSTEST_INTERRUPTHANDLER_H

struct InterruptFrame;

#define INTERRUPT __attribute__((interrupt))

typedef INTERRUPT void(*InterruptHandler)(InterruptFrame* frame);

#endif //OSTEST_INTERRUPTHANDLER_H
