#ifndef OSTEST_MOUSEINTERRUPT_H
#define OSTEST_MOUSEINTERRUPT_H

#include "InterruptHandler.h"

#define MOUSE_INTERRUPT_CODE 0x2C

INTERRUPT void mouseInterrupt(struct InterruptFrame* frame);

#endif //OSTEST_MOUSEINTERRUPT_H
