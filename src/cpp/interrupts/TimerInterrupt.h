#ifndef OSTEST_TIMERINTERRUPT_H
#define OSTEST_TIMERINTERRUPT_H

#include "InterruptHandler.h"

#define TIMER_INTERRUPT_CODE 0x20

INTERRUPT void timerInterrupt(struct InterruptFrame* frame);

#endif //OSTEST_TIMERINTERRUPT_H
