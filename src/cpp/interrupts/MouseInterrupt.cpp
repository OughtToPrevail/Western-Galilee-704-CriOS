#include "MouseInterrupt.h"
#include "System.h"
#include "PICInterrupts.h"

void mouseInterrupt(struct InterruptFrame* frame)
{
	(void) frame;
	System::getSystem().getMouse().alertMouseInterrupt();
	PICInterrupts::getPICInterrupts().acknowledgePICInterrupt(PIC::PIC2);
}