#include "TimerInterrupt.h"
#include "PICInterrupts.h"
#include "System.h"

void timerInterrupt(struct InterruptFrame* frame)
{
	(void) frame;
	System::getSystem().getPIT().alertTick();
	PICInterrupts::getPICInterrupts().acknowledgePICInterrupt(PIC::PIC1);
}
