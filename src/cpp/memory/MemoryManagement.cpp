#include "MemoryManagement.h"

extern unsigned int linkerEnd;  // remove once paging is added

void* operator new(unsigned int size)  // just a stub, add paging later
{
	static char* freeMemoryBase = reinterpret_cast<char*>(linkerEnd + 1000);
	size = (size + 7) / 8 * 8;
	freeMemoryBase += size;
	return freeMemoryBase - size;
}

void* operator new[](unsigned int size)
{
	return operator new(size);
}

void operator delete(void* p)
{
	(void) p;  // only delete when paging is added
}

void operator delete(void* p, unsigned int size)
{
	(void) p;
	(void) size;
}

void operator delete[](void* p)
{
	operator delete(p);
}

void operator delete[](void* p, unsigned int size)
{
	operator delete(p, size);
}
