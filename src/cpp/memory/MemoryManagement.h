#ifndef OSTEST_MEMORYMANAGEMENT_H
#define OSTEST_MEMORYMANAGEMENT_H

void* operator new(unsigned int size);
void* operator new[](unsigned int size);
void operator delete(void* p);
void operator delete(void* p, unsigned int size);
void operator delete[](void* p);
void operator delete[](void* p, unsigned int size);

#endif //OSTEST_MEMORYMANAGEMENT_H
