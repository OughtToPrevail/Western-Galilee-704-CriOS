#include "Logger.h"
#include "VAArgs.h"
#include "String.h"

#define MAX_LOG_LENGTH 1024

Logger::Logger() : _serialPort(COM1)
{

}

void Logger::log(LogType logType, const char* log, ...)
{
	VAArgs args(&log, sizeof(char*));
	char buffer[MAX_LOG_LENGTH];
	String::sprintf(buffer, MAX_LOG_LENGTH, log, args);
	const char* logTypeName;
	switch(logType)
	{
		case LogType::DEBUG:
			logTypeName = "DEBUG";
			break;
		case LogType::INFO:
			logTypeName = "INFO";
			break;
		case LogType::WARNING:
			logTypeName = "WARNING";
			break;
		case LogType::ERROR:
			logTypeName = "ERROR";
			break;
		default:
			logTypeName = "UNKNOWN";
			break;
	}
	this->_serialPort.writeString(logTypeName);
	this->_serialPort.writeString(": ");
	this->_serialPort.writeLine(buffer);
}

