#include "CPPSupport.h"
#include "System.h"
#include "InterruptTable.h"
#include "GDTLoader.h"
#include "Memory.h"

void testLogger();
void testTextMode();
void testGraphicsMode();

extern "C"
{
	int main()
	{
		System& system = System::getSystem();
		system.getUserInterruptHardware().enablePSInterrupts(); // do it before interrupts are actually set up
		Mouse& mouse = system.getMouse();
		mouse.enableMouse();
		unsigned short cs = GDTLoader::loadGDT();
		InterruptTable::registerInterrupts(cs);

		testLogger();
#ifdef GRAPHICS
		testGraphicsMode();
#else
		testTextMode();
#endif
	}
}

void testLogger()
{
	Logger& logger = System::getSystem().getLogger();
	logger.log(LogType::INFO, "Test log");
	logger.log(LogType::DEBUG, "Debug log");
	logger.log(LogType::ERROR, "Error!");
	logger.log(LogType::ERROR, "Error because of %d and %s and %c wooow", 5, "this as well", 'o');
	logger.log(LogType::WARNING, "Size of long is: %d", sizeof(long long));
	logger.log(LogType::INFO, "Process is %s", Memory::isBigEndian() ? "big endian" : "little endian");
}

void testTextMode()
{
	System& system = System::getSystem();
	Shell& shell = system.getShell();
	shell.writeLine("CriOS Shell");
	shell.writeString("> ");

	while(true);
}

void testGraphicsMode()
{
	System& system = System::getSystem();
	system.getGraphicsManager().enable();
	ProgrammableIntervalTimer& timer = system.getPIT();
	while(true)
	{
		system.getGraphicsManager().update();
		timer.wait(10);
	}
}